import {X11Tool} from './X11Tool'
import Path from 'path'
import Os from 'os'
import fs from 'fs'
import uniqid from '/virtual/@kawix/std/util/uniqid'

main()


async function main(){

    let userDir = Path.join(Os.homedir(), ".kawi", "UserData")
    if(!fs.existsSync(userDir))
        fs.mkdirSync(userDir)
    userDir = Path.join(userDir,"zoom-share")
    if(!fs.existsSync(userDir))
        fs.mkdirSync(userDir)


    let file = Path.join(userDir, "zoom.json")
    let started = []
    if(fs.existsSync(file)){
       started = JSON.parse(fs.readFileSync(file,'utf8') )
    }

        console.info(kawix.appArguments)
    let shareType = kawix.appArguments[1] || "video"
    let markVideoOptimize = kawix.appArguments[3] == "markvideo"
    let windows = await X11Tool.search({
        //onlyvisible: true ,
        name: 'Reunión Zoom'
    })

    if(windows[0]){
        // chévere xD
        let win = windows[0]
        let mpvProcess = null
        process.stdin.on("data", function(bytes){
            if(bytes[0] == 3){
                if(mpvProcess) mpvProcess.kill('SIGTERM')
                process.exit(0)
            }
            if(bytes[0] == 4){
                if(mpvProcess) mpvProcess.kill('SIGTERM')
                process.exit(0)
            }
        })
        process.stdin.setRawMode(true)
        process.stdin.resume()



        // resize?

        await X11Tool.sendKey("Escape")


        await X11Tool.mouse.move(800, 570)
        await X11Tool.mouse.click()
        await X11Tool.sendKey("Escape")


        await win.resize(800, 540)
        await win.move(0, 40)
        if(shareType == "video"){
            mpvProcess = X11Tool.exec("mpv", ["--input-terminal=yes", "-no-border", "--title=JW_SHARE_SCREEN", "--no-osc", "--geometry=0:300", kawix.appArguments[2]])
        }
        else{
            mpvProcess = X11Tool.exec("mpv", ["--input-terminal=yes", "-no-border", "--title=JW_SHARE_SCREEN", "-loop", "--no-osc", "--geometry=0:300", kawix.appArguments[2]])
        }
        mpvProcess.stdout.pipe(process.stdout)
        mpvProcess.stderr.pipe(process.stderr)



        windows = await X11Tool.search({
            sync: true,
            name: "JW_SHARE_SCREEN"
        })
        await X11Tool.sleep(500)
        await X11Tool.sendKey("p")
        let mpvWin = windows[0]



        let enableVideoMode = markVideoOptimize/*true
        if(started[0]){
            if((started[0].id == win.id) && (started[0].videoenabled)){
                enableVideoMode = false
            }
        }
        if(shareType == "image"){
            enableVideoMode = !enableVideoMode
        }

        if(markVideoOptimize){
            enableVideoMode = true
        }*/


        let sharewin = null, geo = null, times = 0
        let p_init_share = async function(){
            times++;

            if(sharewin){
                windows = await X11Tool.search({
                    name: "Seleccionar la ventana"
                })
                sharewin  = windows[0]
            }

            if(sharewin && (times > 1)){
                await sharewin.activate()
                await X11Tool.sleep(300)
                return
            }

            //await win.mouse.move(100, 0)
                        await win.activate()
            await X11Tool.sleep(500)
            //await win.mouse.click()
            //await X11Tool.sleep(600)
            await X11Tool.sendKey("alt+s")

            if(!sharewin){
                windows = await X11Tool.search({
                    sync: true,
                    name: "Seleccionar la ventana"
                })
                sharewin  = windows[0]
            }
            await mpvWin.move(0,800)
            console.info("---- READY TO SHARE -----")
        }
        setTimeout(p_init_share, 1500)

        let p_share = async function(){
            await p_init_share()
            geo = await sharewin.getGeometry()
            await X11Tool.mouse.move(geo.location.x+40, geo.location.y+80)
            await win.mouse.click()
            await X11Tool.sleep(400)


            if(enableVideoMode){
                //await X11Tool.mouse.move(geo.location.x+200, geo.location.y+ (geo.size.height - 10))
                await sharewin.mouse.move(200, geo.size.height - 12)
                await X11Tool.sleep(500)
                await win.mouse.click()
            }
            else{
                await sharewin.mouse.move(210, geo.size.height - 12)
                await X11Tool.sleep(250)
                await win.mouse.click()
                await X11Tool.sleep(250)
                await win.mouse.click()
            }
        }

        let p_ok = async function(){
            //await X11Tool.mouse.move(geo.location.x + (geo.size.width - 20), geo.location.y+ (geo.size.height - 10))
            geo = await sharewin.getGeometry()
            await sharewin.mouse.move(geo.size.width - 20, geo.size.height - 12)
            await X11Tool.sleep(200)
            await win.mouse.click()
            await X11Tool.sleep(1000)
            // MPV
            await mpvWin.activate()
            await X11Tool.sleep(100)
        }

        let p_force = async function(){

            await win.mouse.move(100,10)
            await win.mouse.click()
            await X11Tool.sleep(500)
            await X11Tool.sendKey("Escape")
            sharewin = null
            geo = null
            await p_share()

            await X11Tool.sleep(200)
            await X11Tool.sendKey("Right")
            await X11Tool.sleep(500)
            await p_ok()

        }

        let p_start_ok = false
        let p_start = async function(){
            if(!p_start_ok){
                p_start_ok = true
                //await sharewin.activate()
                await p_share()

                await X11Tool.sleep(200)
                await X11Tool.sendKey("Right")
                await X11Tool.sleep(200)
                await p_ok()

                await X11Tool.sendKey("p")

                started = [
                    {
                        id: win.id,
                        started: Date.now(),
                        videoenabled: shareType != "image"
                    }
                ]
                fs.writeFileSync(file,JSON.stringify(started))

            }else{
                await mpvWin.activate()
                await X11Tool.sendKey("p")
            }
        }


        process.stdin.on("data", async function(bytes){
            if(bytes.toString() == "m"){
                await p_start()
            }
            else if(bytes.toString() == "o"){
                await p_force()
            }
            else if(bytes.toString() == "b"){
                await mpvWin.activate()
                await X11Tool.sendKey("Left")
            }
            else if(bytes.toString() == "f"){
                await mpvWin.activate()
                await X11Tool.sendKey("Right")
            }
            else{
                await mpvWin.activate()
                await X11Tool.sendKey(bytes.toString())
            }
            //mpvProcess.stdin.write(bytes)
        })




    }


}
